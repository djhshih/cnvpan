#!/bin/bash

make -f Makefile,genome=hg19,table=cytoBand
make -f Makefile,genome=hg19,table=refGene
make -f Makefile,genome=hg38,table=cytoBand
make -f Makefile,genome=hg38,table=refGene

./cytoband.r
