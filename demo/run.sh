#!/bin/bash

# prepare data files
cd data/ucsc/
./make.sh
cd -

# run analysis
Rscript prepare.r
Rscript run.r
