# ==============================================================================
# PURPOSE
# Copy-number event frequency analysis and plotting

# ==============================================================================
# PREAMBLE
# 

library(io);
library(RColorBrewer);
library(ggplot2);
library(MASS);
library(RSQLite);
library(dplyr);

source("../R/utils.R");
source("../R/broad.R");
source("../R/pipeline.R");

cytoband <- qread("data/ucsc/hg19/cytoBand.rds");
genesDb.fname <- "data/ucsc/hg19/refGene.db";

input.fnames <- "ep_cna.mtx";

groups.fname <- "data/ep/subtypes.tsv";


output.fstem <- "ep_cna";

chrom.arms.only <- TRUE;
rm.chrX <- TRUE;


# ==============================================================================
# INPUT
# 

X <- qread(input.fnames);

pheno <- qread(groups.fname);

chroms <- rownames(X);

if (chrom.arms.only) {
	# remove whole chromosomes
	chroms <- grep("p|q", chroms, value=TRUE);
}

if (rm.chrX) {
	# remove the X chromosome
	chroms <- chroms[chroms != "Xp" & chroms != "Xq" & chroms != "X"];
}

chrom.arms.df <- cytoband$chrom.arms;
rownames(chrom.arms.df) <- with(chrom.arms.df, paste(chromosome, arm, sep=""));

chrom.ngenes <- count_genes_on_chroms(chrom.arms.df[chroms, ],  genesDb.fname);

wchroms <- as.character(1:22);

greek.letters <- c("alpha", "beta", "gamma", "delta", "epsilon", "zeta", "eta", "theta", "iota", "kappa", "lambda", "mu", "nu", "xi", "omicron", "pi", "rho", "sigma", "tau", "upsilon", "phi", "chi", "psi", "omega")


####

options(filenamer.path.timestamp=0);
options(filenamer.timestamp=0);


partitions <- colnames(pheno)[-1];

for (partition in partitions) {

	message(partition);

	path <- file.path("all", sub("_", "", partition));

	groups <- pheno %>%
		select(sample, group_idx=starts_with(partition));

	groups$group <- with(groups,
		factor(
			greek.letters[group_idx],
			levels = greek.letters[1:max(group_idx)]
		)
	);

	cl <- groups$group[match(colnames(X), groups$sample)];

	output.fname <- filename(path=path, output.fstem);
	run_pipeline(X, cl, output.fname, chroms, wchroms, chrom.ngenes);

	graphics.off();

}
