
run_pipeline <- function(X, cl, output.fname, chroms, wchroms, chrom.ngenes, q.cut=0.1, ...) {

	# ==============================================================================
	# PREAMBLE
	# 

	valid.idx <- !is.na(cl);
	X <- X[, valid.idx];
	cl <- cl[valid.idx];

	nsamples <- ncol(X);
	ngenes <- nrow(X);

	group.labels <- levels(cl);
	names(group.labels) <- group.labels;

	X.chrom.arms <- X[chroms, ];
	X.wchroms <- X[wchroms, ];

	out.fname.csv <- insert(output.fname, ext="csv");
	out.fname.mtx <- insert(output.fname, ext="mtx");
	out.fname.rds <- insert(output.fname, ext="rds");
	out.fname.freq.pdf <- insert(output.fname, tag="freq", ext="pdf");

	# ==============================================================================
	# PROCESS
	# 

	## Calculate lesion frequencies across all samples

	#counts <- lapply(cn.types, function(typev) apply(extreme(X, typev), 1, sum));
	#freqs <- lapply(counts, function(x) x/nsamples);

	freq.stats <- chrom_freq_tests(X, cl, q.cut=q.cut, ...);

	sig.events <- extract_events(freq.stats);

	Y <- make_event_matrix(X, sig.events);

	Y.groups <- lapply(group.labels, function(group) {
		make_event_matrix(X[, cl == group], sig.events[, group, drop=FALSE])
	});


	# ==============================================================================
	# OUTPUT
	# 

	for (i in 1:length(freq.stats)) {
		qdraw(freq_regression_plot(freq.stats[[i]]),
			file=insert(output.fname, ext="pdf", tag=c(tag2path(group.labels[i]), "rlm")));
	}

	# write frequency statistics
	discard <- lapply(group.labels, function(group) {
		qwrite(freq.stats[[group]], insert(out.fname.rds, tag=c(tag2path(group), "stats")));
		qwrite(freq.stats[[group]]$summary, insert(out.fname.csv, tag=c(tag2path(group), "stats")));
	});

	# write subgroup chrom status
	qwrite(sig.events, insert(out.fname.mtx, "chrom-cna"));

	if (!is.null(Y)) {
		# write arm-level event matrix
		qwrite(Y, tag(out.fname.mtx, tag="regions"));
	}

	# write arm-level event matrix for each subgroup
	discard <- lapply(group.labels, function(g) {
		if (!is.null(Y.groups[[g]])) {
			fname.group <- insert(output.fname, ext="mtx");
			fname.group$fstem <- paste(fname.group$fstem, tag2path(g), sep="-");
			qwrite(Y.groups[[g]], insert(fname.group, "regions"));
		}
	});


	# ==============================================================================
	# PLOTS
	# 

	opts <- options(plot=list(width=3, height=6.5));

	freq.gains <- get_frequency(X.chrom.arms, 1);
	freq.losses <- get_frequency(X.chrom.arms, -1);

	g <- chrom_freq_gain_plot(freq.gains) + scale_y_continuous(limits=c(0, 0.5));
	qdraw(g, file=insert(out.fname.freq.pdf, tag="gain"));

	g <- chrom_freq_loss_plot(freq.losses) + scale_y_continuous(limits=c(0, 0.5));
	qdraw(g, file=insert(out.fname.freq.pdf, tag="loss"));


	freq.gains.chr <- get_frequency(X.wchroms, 1);
	freq.losses.chr <- get_frequency(X.wchroms, -1);

	g <- chrom_freq_gain_plot(freq.gains.chr) + scale_y_continuous(limits=c(0, 0.5));
	qdraw(g, file=insert(out.fname.freq.pdf, tag="gain-wchrom"));

	g <- chrom_freq_loss_plot(freq.losses.chr) + scale_y_continuous(limits=c(0, 0.5));
	qdraw(g, file=insert(out.fname.freq.pdf, tag="loss-wchrom"));


	options(plot=list(width=8, height=6.5));

	freq.group.gains <- get_summary_group(freq.stats, "gain");
	freq.group.losses <- get_summary_group(freq.stats, "loss");

	g <- chrom_freq_gain_group_plot(freq.group.gains);
	qdraw(g, file=insert(out.fname.freq.pdf, tag=c("group", "gain")));

	g <- chrom_freq_loss_group_plot(freq.group.losses);
	qdraw(g, file=insert(out.fname.freq.pdf, tag=c("group", "loss")));


	freq.group.gains.chr <- get_frequency_group(X.wchroms, 1, cl);
	freq.group.losses.chr <- get_frequency_group(X.wchroms, -1, cl);

	g <- chrom_freq_gain_group_plot(freq.group.gains.chr);
	qdraw(g, file=insert(out.fname.freq.pdf, tag=c("group", "gain-wchrom")));

	g <- chrom_freq_loss_group_plot(freq.group.losses.chr);
	qdraw(g, file=insert(out.fname.freq.pdf, tag=c("group", "loss-wchrom")));

	# reset options
	options(opts);

	freq.stats
}
