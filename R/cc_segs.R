# Remove extreme segments
rm_extreme_segs <- function(seg, logr.cut) {
	seg[abs(seg$logr) < logr.cut, ]
}

# normalize a seg data.frame
nm_seg <- function(seg) {
	colnames(seg) <- c("sample", "chromosome", "start", "end", "nprobes", "logr");
	seg
}

# Generate random sign
rsign <- function(n) {
	ifelse(runif(n) > 0.5, 1, -1)
}

# Generate permuted case-control segs without any signal
gen_perm_cc_segs <- function(case, control, seed=1, logr.cut=3) {
	set.seed(seed);
	
	case.f <- rm_extreme_segs(case, logr.cut=logr.cut);
	control.f <- rm_extreme_segs(control, logr.cut=logr.cut);

	cases.by.sample <- split(case.f, case.f$sample);
	controls.by.sample <- split(control.f, control.f$sample);
	
	n.cases <- length(cases.by.sample);
	n.controls <- length(controls.by.sample);

	# permute the case and controls to mix them together
	combined.by.sample <- c(cases.by.sample, controls.by.sample);
	perm.idx <- sample(1:length(combined.by.sample));
	combined.by.sample <- combined.by.sample[perm.idx];

	# re-label the first set of samples as case and
	#         the second set of samples as control
	group <- c(rep(1, n.cases), rep(0, n.controls));
	s.cases <- do.call(rbind, combined.by.sample[group == 1]);
	s.controls <- do.call(rbind, combined.by.sample[group == 0]);

	structure(
		list(
			case = s.cases,
			control = s.controls,
			n = list(
				case = n.cases,
				control = n.controls
			)
		),
		class = "cc_segs"
	)
}

# Generate recurrent segments
gen_rseg <- function(n, seg, n.cases, chroms, chr.starts, chr.ends, genome, params=NULL) {

	# genome sizes of recurrent events
	rseg.sizes <- rnorm(n, 10e6, 2e6);

	# logR of recurrent events
	rseg.sign <- rsign(n);
	rseg.logr <- rseg.sign * rlnorm(n, params$logr.lmean, params$logr.lsd);

	# frequencies of recurrent event
	rseg.freqs <- rbeta(n, params$freq.alpha, params$freq.beta);
	rseg.ns <- round(n.cases * rseg.freqs);

	rseg.chroms <- sample(chroms, n, replace=TRUE);
	rseg.starts <- round(runif(n, chr.starts[rseg.chroms], chr.ends[rseg.chroms] - rseg.sizes));
	rseg.ends <- rseg.starts + rseg.sizes - 1;

	rseg <- data.frame(
		type = ifelse(rseg.sign > 0, "Amp", "Del"),
		chromosome = rseg.chroms,
		start = rseg.starts, end = rseg.ends,
		size = rseg.sizes,
		logr = rseg.logr,
		freq = rseg.freqs,
		n = rseg.ns
	);

	# filter out centromere regions
	rseg.f <- cngpld:::filter_centromere_regions(rseg, genome) %>%
		# ensure enough events are observed
		filter(is.finite(size));

	rseg.f.chrom <- split(rseg.f, list(chromosome=rseg.f$chromosome, direction=rseg.f$logr > 0));
	dups <- lapply(rseg.f.chrom,
		function(d) {
			n <- nrow(d);
			if (n > 1) {
				unique(unlist(lapply(1:(n-1),
					function(i) {
						# look for overlaps after the current region
						# adjust index s.t. we report the row index
						which(overlap(d$start[i], d$end[i], d$start[(i+1):n], d$end[(i+1):n])) + i
					}
				)))
			}
		}
	);

	# Remove overlapping events
	rseg.f <- do.call(rbind, mapply(
		function(d, dup) {
			if (length(dup) > 0) {
				d[-dup, ]
			} else {
				d
			}
		},
		rseg.f.chrom,
		dups,
		SIMPLIFY=FALSE
	));
	rownames(rseg.f) <- NULL;

	rseg.f
}

# Generate surrogate case-control segs from permuted segs by spiking in signals
# @param n.events  number recurrent events
# TODO Generate true negative events (not covered by any signal segments)
gen_surr_cc_segs <- function(perm, seed=1, n.events=50, genome="hg19", params=NULL) {
	set.seed(seed);

	if (is.null(params)) {
		params <- list();
	}
	if (is.null(params$logr.lmean)) {
		params$logr.lmean <- 0.2;
	}
	if (is.null(params$logr.lsd)) {
		params$logr.lsd <- 0.5;
	}
	if (is.null(params$freq.alpha)) {
		params$freq.alpha <- 10;
	}
	if (is.null(params$freq.beta)) {
		params$freq.beta <- 40;
	}
	params$seed <- seed;


	seg <- perm$case;

	samples <- unique(seg$sample);

	chroms <- setdiff(unique(seg$chromosome), c("X", "Y"));
	names(chroms) <- chroms;

	# minimum start positions on each chromosome
	chr.starts <- unlist(lapply(chroms, function(chrom) min(seg$start[seg$chromosome == chrom])));
	# maximum end positions on each chromosome
	chr.ends <- unlist(lapply(chroms, function(chrom) max(seg$end[seg$chromosome == chrom])));

	rseg.f <- gen_rseg(n.events, seg=perm$case, n.cases=perm$n$case,
		chroms=chroms, chr.starts=chr.starts, chr.ends=chr.ends, genome=genome, params=params);

	# generate segments within samples for each recurrent segment
		
	# list of segment property vectors for each recurrent event
	seg.samples <- lapply(rseg.f$n, function(n) sample(samples, n));

	seg.chroms <- mapply(
		function(n, chrom) {
			rep(chrom, n);
		},
		rseg.f$n,
		rseg.f$chromosome,
		SIMPLIFY = FALSE
	);

	seg.starts <- mapply(
		function(n, s, size, chr.start) {
			pmax(chr.start, s - round(runif(n, 0, size * 0.5)))
		},
		rseg.f$n,
		rseg.f$start,
		rseg.f$size,
		chr.starts[rseg.f$chromosome],
		SIMPLIFY = FALSE
	);

	seg.ends <- mapply(
		function(n, e, size, chr.end) {
			pmin(chr.end, e + round(runif(n, 0, size * 0.5)))
		},
		rseg.f$n,
		rseg.f$end,
		rseg.f$size,
		chr.ends[rseg.f$chromosome],
		SIMPLIFY = FALSE
	);

	seg.sizes <- mapply(function(s, e) e - s + 1, seg.starts, seg.ends);

	seg.logr <- mapply(
		function(logr, rsize, sizes) {
			# reduce logr porportional to size of enlargement
			logr * rsize / sizes
		},
		rseg.f$logr,
		rseg.f$size,
		seg.sizes,
		SIMPLIFY = FALSE
	);

	seg.insert <- data.frame(
		sample = unlist(seg.samples),
		chromosome = unlist(seg.chroms),
		start = unlist(seg.starts),
		end = unlist(seg.ends),
		nprobes = 100,
		logr = unlist(seg.logr)
	);

	surrogate <- perm;

	# NB  signal segments overlap with existing segments
	seg.sur <- rbind(seg, seg.insert);
	seg.sur <- with(seg.sur, seg.sur[order(sample, chromosome, start, end), ]);

	surrogate$case <- seg.sur;
	surrogate$events <- rseg.f;
	surrogate$params <- params;

	surrogate
}

