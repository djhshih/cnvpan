
cn_count_enrich_test <- function(count, n.cases, n.controls, direction=1) {
	res <- mapply(
		function(x.case, x.control) {
			fisher.test(
				count.ct <- matrix(
					c(
						n.controls - x.control,
						n.cases - x.case,
						x.control,
						x.case
					),
					nrow = 2
				),
				alternative = ifelse(direction > 0, "greater", "less")
			)
		},
		count$case,
		count$control,
		SIMPLIFY = FALSE
	);

	estimates <- unlist(lapply(res, function(r) r$estimate));
	ps <- unlist(lapply(res, function(r) r$p.value));

	data.frame(
		position = count$position,
		n_cna_case = count$case,
		n_cna_control = count$control,
		odds_ratio = estimates,
		p = ps
	)
}

postprocess_enrich_res <- function(res, region=TRUE) {
	d <- do.call(rbind,
		mapply(function(d, chrom) data.frame(chromosome = chrom, d), res, names(res), SIMPLIFY=FALSE)
	);
	d$q <- p.adjust(d$p, method="BH");
	rownames(d) <- NULL;
	d
}

find_enrich_regions <- function(d, fdr) {
	do.call(rbind, lapply(split(d, d$chromosome),
		function(dd) {
			idx <- which(dd$q < fdr);
			intervals <- gpldiff:::find_contiguous(idx);
			if (is.null(intervals) || nrow(intervals) == 0) {
				NULL
			} else {
				do.call(rbind, lapply(1:nrow(intervals),
					function(i) {
						interval <- intervals[i, ];
						# select optimal entry
						i <- interval$start + which.min(dd$p[interval$start:interval$end]) - 1;
						cbind(dd[i, ], start = dd$position[interval$start], end = dd$position[interval$end], n_obs = interval$n)
					}
				))
			}
		}
	))
}
