#' Evaluation predictions
#'
#' This function evaluates predictions
#'
#' @param x predictions
#' @param labels class labels
#' @return a \code{predperf} object object
#' 
#' @export
#'
eval_predictions <- function(x, labels) {
	# ensure labels are binary and convert to (0, 1)
	labels <- as.numeric(labels);
	stopifnot(length(unique(labels)) == 2);
	labels[which.min(labels)] <- 0;
	labels[which.max(labels)] <- 1;

	# sort predictions and labels
	# larger x predicts high probability of positive labels
	idx <- order(x, decreasing=TRUE);
	x <- x[idx];
	labels <- labels[idx];

	n <- length(x);
	nn <- 1:n;

	# accumulate positive labels in forward direction
	tp <- cumsum(labels);
	fp <- nn - tp;
	# prepend with 0, since we start with the first element predicted as positive
	tp <- c(0, tp);
	fp <- c(0, fp);

	# accumulate negative labels in reverse direction
	rev_labels_0 <- rev(labels == 0);
	tn <- cumsum(rev_labels_0);
	fn <- nn - tn;
	# prepend with 0, since we start with the first element predicted as negative
	# then, reverse the sequence since we worked in the reverse direction
	tn <- rev(c(0, tn));
	fn <- rev(c(0, fn));

	n_pos_labels <- tp[n+1];
	n_neg_labels <- tn[1];

	# undo the sort order
	x[idx] <- x;
	labels[idx] <- labels;
	tp[idx] <- tp;
	fp[idx] <- fp;
	tn[idx] <- tn;
	fn[idx] <- fn;

	# sensitivity (true positive rate)
	sensitivity <- tp / (tp + fn);

	# specificity (1 - false positve rate)
	specificity <- tn / (fp + tn);

	# precision (1 - false discovery rate)
	precision <- tp / (tp + fp);

	# recall
	recall <- sensitivity;
	
	auc.roc <- auc_roc(specificity, sensitivity);
	auc.pr <- auc_pr(tp, fp, fn)$auc;

	structure(
		list(
			x = x,
			labels = labels,
			tp = tp,
			fp = fp,
			tn = tn,
			fn = fn,
			n_pos_labels = n_pos_labels,
			n_neg_labels = n_neg_labels,
			sensitivity = sensitivity,
			specificity = specificity,
			precision = precision,
			recall = recall,
			auc_roc = auc.roc,
			auc_pr = auc.pr
		),
		class = "predperf"
	)
}

# confidence interval
agresti_coull_cint <- function(x, n, alpha=0.05) {
	z <- qnorm(1 - alpha/2);
	z2 <- z*z;

	np <- n + z2;
	pp <- 1/np * (x + z2 / 2);
	d <- z * sqrt(pp / np * (1 - pp));

	matrix(
		c(pmax(0, pp - d), pmin(1, pp + d)),
		ncol = 2,
		dimnames = list(names(x), c("lower", "upper"))
	)
}

#' Combine prediction performance results
#'
#' This combines a list of prediction performance lists
#'
#' @param a list of predpref objects
#' @return a \code{predpref_list} object
#' @export
#'
combine_predperf <- function(x) {
	if (!is.list(x)) {
		stop("x must be a list of predperf objects")
	}
	# remove NULL elements
	x <- x[ !unlist(lapply(x, is.null)) ];

	# remove non-predpref objects
	idx <- unlist(lapply(x, function(z) class(z) != "predperf"));
	if (any(idx)) {
		warning("Non-predpref objects are removed");
	}
	x <- x[!idx];

	if (is.null(names(x))) {
		names(x) <- LETTERS[1:length(x)];
	}

	structure(
		x,
		class = "predperf_list"
	)
}

#' Plot prediction performance
#'
#' This function plots the performance of a prediction result.
#'
#' @param x predpref object
#' @param type precision recall or ROC
#' @return a ggplot object
#'
#' @import ggplot2
#' @export
#'
plot.predperf <- function(x, type=c("pr", "roc")) {

	type <- match.arg(type);

	if (type == "roc") {
		d <- data.frame(
			y = c(0, x$sensitivity, 1),
			x = c(0, 1 - x$specificity, 1)
		);
	} else {
		d <- data.frame(
			y = c(1, x$precision, 0),
			x = c(0, x$recall, x$recall[length(x$recall)])
		);
	}

	g <- ggplot(d, aes(x=x, y=y));

	if (type == "roc") {
		g <- g + xlab("1 - Specificity") + ylab("Sensitivity");
	} else {
		g <- g + xlab("Recall") + ylab("Precision");
	}

	g + geom_line() + theme_classic() + xlim(0, 1) + ylim(0, 1)
}

#' Plot prediction performances
#'
#' This function plots a list of prediction performances
#'
#' @param x predpref object
#' @param type precision recall or ROC
#' @return a ggplot object
#'
#' @import ggplot2
#' @export
#'
plot.predperf_list <- function(x, type=c("pr", "roc"), se=TRUE) {
	type <- match.arg(type);

	ds <- lapply(x,
		function(x) {
			if (type == "roc") {
				d <- data.frame(
					y = c(0, x$sensitivity, 1),
					x = c(0, 1 - x$specificity, 1)
				);
			} else {
				d <- data.frame(
					y = c(1, x$precision, 0),
					ymin = c(1, x$cint$precision[,1], 0),
					ymax = c(1, x$cint$precision[,2], 0),
					x = c(0, x$recall, x$recall[length(x$recall)])
				);
			}
		}
	);

	groups <- names(x);

	d <- data.frame(ds[[1]], group=groups[1]);
	if (length(ds) > 1) {
		for (i in 2:length(ds)) {
			d <- rbind(d, data.frame(ds[[i]], group=groups[i]));
		}
	}
	d$group <- factor(d$group, levels=names(x));

	g <- ggplot(d, aes(x=x, y=y, ymin=ymin, ymax=ymax)) +
		theme_classic();

	if (type == "roc") {
		g <- g + xlab("1 - Specificity") + ylab("Sensitivity");
	} else {
		g <- g + xlab("Recall") + ylab("Precision");
	}

	if (se) {
		g <- g + geom_ribbon(aes(fill=group), show.legend=FALSE, alpha=0.2);
	}

	g + geom_line(aes(colour=group)) + 
		xlim(0, 1) + ylim(0, 1)
}

# @param x  a list of list of \code{predperf} objects
plot.predperf_lists <- function(x, type=c("pr", "roc")) {
	type <- match.arg(type);
	xss <- x;

	d <- do.call(rbind, mapply(
		function(xs, group) {
			mapply(
				function(x, i) {
					if (type == "roc") {
						data.frame(
							y = c(0, x$sensitivity, 1),
							x = c(0, 1 - x$specificity, 1),
							i = i,
							group = group
						)
					} else {
						data.frame(
							y = c(1, x$precision, 0),
							x = c(0, x$recall, x$recall[length(x$recall)]),
							i = i,
							group = group
						)
					}
				},
				xs,
				1:length(xs),
				SIMPLIFY = FALSE
			)
		},
		xss,
		names(xss)
	));

	g <- ggplot(d, aes(x=x, y=y, colour=group));
	if (type == "roc") {
		g <- g + xlab("1 - Specificity") + ylab("Sensitivity");
	} else {
		g <- g + xlab("Recall") + ylab("Precision");
	}

	g + geom_line(aes(group=i), alpha=0.2) + theme_classic() + xlim(0, 1) + ylim(0, 1)
}

# use the trapezoid method to calculate area under the receiver-operating
# characteristic curve with linear interpolation between points
auc_roc <- function(specificity, sensitivity) {
	trapezoid_integration(1 - specificity, sensitivity)
}

# use the trapezoid method to calculate area under the precision-recall
# curve after interpolation between points using the Davis-Goadrich method
auc_pr <- function(tp, fp, fn) {
	n <- length(tp);
	d <- diff(tp);

	# we need to interpolate between every gap in tp
	# gap of size d - 1 occurs whenever d > 1
	# therefore the new number of points is the original number of points
	# plus the number of points to be interpolated
	gap <- d > 1;
	ninter <- ifelse(gap, d - 1, 0);
	n2 <- n + sum(ninter);

	tp2 <- integer(n2);
	fp2 <- integer(n2);
	fn2 <- integer(n2);

	if (n > 0) {
		k <- 1;
		for (i in 1:n) {
			# copy old points to new vectors
			tp2[k] <- tp[i];
			fp2[k] <- fp[i];
			fn2[k] <- fn[i];
			k <- k + 1;
			if (i < n && gap[i]) {
				# interpolate points in gap
				m <- ninter[i];
				idx <- k:(k+m-1);
				mm <- 1:m;
				tp2[idx] <- tp[i] + mm;
				fn2[idx] <- fn[i] - mm;
				local_skew <- (fp[i+1] - fp[i]) / (tp[i+1] - tp[i]);
				fp2[idx] <- fp[i] + (mm * local_skew);
				k <- k + m;
			}
		}
	}

	recall <- tp2 / (tp2 + fn2);
	precision <- tp2 / (tp2 + fp2);

	recall.cint <- agresti_coull_cint(tp2, tp2 + fn2);
	precision.cint <- agresti_coull_cint(tp2, tp2 + fp2);

	# define first precision as 1 for calculating AUC
	if (n > 0 && is.na(precision[1])) precision[1] <- 1;
	auc <- trapezoid_integration(recall, precision);

	structure(
		list(
			tp = tp2,
			fp = fp2,
			fn = fn2,
			recall = recall,
			precision = precision,
			cint = list(
				recall = recall.cint,
				precision = precision.cint
			),
			auc = auc
		),
		class = "predperf"
	)
}

# Combine multiple confusion lists
# assuming that the thresholds are the same
# @params prs  list of list(threshold, tp, fp, fn)
combine_confusions <- function(prs) {
	n <- length(prs[[1]]$tp);

	tp <- numeric(n);
	fn <- numeric(n);
	fp <- numeric(n);

	for (i in 1:length(prs)) {
		tp <- tp + prs[[i]]$tp;
		fn <- fn + prs[[i]]$fn;
		fp <- fp + prs[[i]]$fp;
	}
	
	list(
		threshold = prs[[1]]$threshold,
		tp = tp,
		fn = fn,
		fp = fp
	)
}

trapezoid_integration <- function(x, y) {
	dx <- diff(x);

	# (left side + right side) * width / 2
	# then, sum areas of all trapezoids
	sum( (y[-length(y)] + y[-1]) * dx / 2 )
}

